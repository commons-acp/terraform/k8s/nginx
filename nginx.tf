
resource "kubernetes_deployment" "nginx" {
  depends_on = [kubernetes_secret.docker-cfg]
  metadata {
    name = "nginx"
    labels = {
      app = "nginx"
    }
  }
  spec {
    selector {
      match_labels = {
        app = "nginx"
      }
    }

    template {
      metadata {
        labels = {
          app = "nginx"
        }
      }
      spec {
        container {
//          image = "nginx:1.7.8"
          image = var.gitlab_image_url
          name = "nginx"
          port {
            container_port = 80
          }


        }
        image_pull_secrets {
          name= "docker-cfg"
        }
      }
    }
  }
}


resource "kubernetes_service" "nginx-service" {
  depends_on = [kubernetes_deployment.nginx]
  metadata {
    name      = "nginx-service"
  }
  spec {
    selector = {
      app = "nginx"
    }
    type = "NodePort"
    port {
      node_port   = 30201
      protocol = "TCP"
      port = 80
      target_port = 80
    }
  }
}


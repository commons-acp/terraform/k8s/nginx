variable "gitlab_api_token" {
  description = ""
}
variable "gitlab_image_url" {
  description = ""
}
variable "registry_server" {
  description = ""
}
variable "registry_username" {
  description = ""
}
variable "registry_password" {
  description = ""
}
